// @ts-check
const style = document.createElement("style");
style.textContent = `
:host {
    all: initial; /* 1st rule so subsequent properties are reset. */
    display: block;
    background: white;

    --animation-duration: 13s;
    --animation-state: running;
}

.delimiter {
    font-size: 0.8rem;
    margin: 0 1rem 0 1rem;
}

.news-feed-container {
    font-family: sans-serif;
    overflow: hidden;
    height: 40px;
    width: 100%;
    position: relative;
    background-color: #333236;
    display: flex;
    flex-flow: row wrap;
    white-space: nowrap;
}

.container-wrapper div {
   animation-play-state: var(--animation-state);
}

.news-feed-one {
    box-sizing: border-box;
    height: 100%;
    display:flex;
    position:relative;
    transition: transform 0.3s;
    animation:slide-one var(--animation-duration) linear infinite;
}

.news-feed-two {
    position: absolute;
    top: 0;
    box-sizing: border-box;
    height: 100%;
    display:flex;
    transition: transform 0.3s;
    animation:slide-two var(--animation-duration) linear infinite;
}

.news-feed-item {
    -webkit-font-smoothing: antialiased;
    text-decoration: none;
    display: inline-flex;
    padding: 0 5px 0 5px;
    font-size: 1em;
    font-weight: 400;
    color: #fff;
    float: left;
    transform: translateY(-20%);
    position: relative;
    top: 50%;
}

.sentiment-bar-container {
     width: 100px;
    height: 5px;
        margin: 0 0.5rem 0 0.5rem;
    transform: translateY(0.4rem);
    background-color: #7C7B7C;
    border-radius: 2px;
 }

.sentiment-bar {
    border-radius: 2px;
    background-color: #7C7B7C;
    width: 100%;
    height: 100%;
    position: relative;
}

.sentiment-bar-positive {
    border-radius: 2px;
    background-color: #53B987;
    height: 100%;
    position: relative;
}

.sentiment-bar-negative {
    border-radius: 2px;
    background-color: #EB4D5C;
    height: 100%;
    position: relative;
}

@keyframes slide-one { 
  0%{
      transform: translateX(0%);
  }
  100%{
      transform: translateX(-100%);
  }
}

@keyframes slide-two { 
  0%{
      transform: translateX(100%);
  }
  100%{
      transform: translateX(0%);
  }
}

.crypto-logo {
    position: absolute;
    padding: 0.4rem 0 0.0rem 1rem;
}

.crypto-mask {
    position: absolute;
    height: 40px;
}
`;

const fetch = url => body =>
  new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open("POST", url);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.addEventListener("readystatechange", () => {
      if (xhr.readyState !== 4) {
        return;
      }

      if (xhr.status >= 200 && xhr.status < 300) {
        resolve(JSON.parse(xhr.response));
      } else {
        reject(new Error("Connection error"));
      }
    });

    xhr.send(JSON.stringify(body));
  });

const fetchGql = fetch("http://pro.cryptomood.com/graphql");

const getNews = assetId =>
  fetchGql({
    query: `
        query {
          daily(currencyId: "${assetId}") {
            news(limit: 10) {
              id
              weight
              title
              link
              timestamp
              impact
            }
          }
        }
      `
  });

const getAssets = () =>
  fetchGql({
    query: `
      query {
        currencies {
          id
          code
        }
      }
    `
  });

const cachedGetAssets = (() => {
  let assets = null;

  return async () => {
    if (assets != null) {
      return assets;
    }

    const response = await getAssets();
    assets = response.data.currencies;

    return assets;
  };
})();

function handleLogoInsertion(container) {
  const cryptoMask = document.createElement("div");
  cryptoMask.className = "crypto-mask";
  cryptoMask.id = "crypto-mask";
  cryptoMask.innerHTML = `
    <svg width="305" height="40" viewBox="0 0 305 40" fill="none" xmlns="http://www.w3.org/2000/svg">
      <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="305" height="40">
      <rect width="305" height="40" fill="#212121"/>
      </mask>
      <g mask="url(#mask0)">
      <g filter="url(#filter0_d)">
      <rect y="-1" width="186.328" height="42" fill="#212121"/>
      </g>
      </g>
      <defs>
      <filter id="filter0_d" x="-4" y="-35" width="254.328" height="110" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
      <feFlood flood-opacity="0" result="BackgroundImageFix"/>
      <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
      <feOffset dx="30"/>
      <feGaussianBlur stdDeviation="17"/>
      <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0"/>
      <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
      <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
      </filter>
      </defs>
      </svg>
`;
  container.appendChild(cryptoMask);

  const cryptoLogo = document.createElement("div");
  cryptoLogo.className = "crypto-logo";
  cryptoLogo.id = "crypto-logo";
  cryptoLogo.innerHTML = `
          <svg width="157" height="27" viewBox="0 0 157 27" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M27.9719 14.8274H16.6057C16.1018 14.8274 15.6348 15.0978 15.3838 15.5356L11.9382 21.565C11.8973 21.635 11.8234 21.6798 11.7417 21.6798H11.4323C10.6697 21.6798 9.96341 21.2732 9.58207 20.6117L9.4809 20.4366C9.44004 20.3665 9.44004 20.2809 9.4809 20.2109L12.9382 14.1367C13.1873 13.7028 13.1873 13.1698 12.9382 12.7339L9.45561 6.67534C9.41475 6.6053 9.41475 6.51969 9.45561 6.44965L9.58207 6.22981C9.96146 5.5722 10.6638 5.16751 11.4226 5.16751H11.6269C11.7067 5.16751 11.7806 5.21031 11.8214 5.27841L15.3644 11.302C15.6154 11.7397 16.0823 12.0102 16.5862 12.0102H27.9719C28.7209 12.0102 29.1898 11.1989 28.8143 10.551L23.7266 1.74909L23.7168 1.73157C23.711 1.72185 23.7052 1.71213 23.6993 1.7024C23.6429 1.60706 23.5826 1.51561 23.5184 1.42806C22.8705 0.538924 21.8277 0 20.7148 0H9.20851C9.06843 0 8.92835 0.00973084 8.79021 0.0272412C7.84271 0.140086 6.97886 0.649829 6.41658 1.42223C6.34459 1.51951 6.2765 1.62263 6.21618 1.72769L0.463053 11.693C0.192615 12.16 0.0408592 12.6756 0.00778405 13.197C0.00389286 13.2709 0 13.3468 0 13.4207C0 14.018 0.153703 14.6134 0.463053 15.1484L6.21618 25.1138C6.29595 25.2519 6.38545 25.3842 6.48079 25.5068C7.08198 26.2811 7.98668 26.7734 8.9731 26.8415C9.05092 26.8473 9.13069 26.8492 9.20851 26.8492H9.22992H20.7031C20.9833 26.8492 21.2576 26.8142 21.5222 26.75C22.2849 26.5652 22.9717 26.1216 23.4561 25.4971C23.5456 25.3823 23.6273 25.2616 23.7013 25.1352C23.7071 25.1254 23.7129 25.1177 23.7188 25.1079L23.7227 25.1002L28.8162 16.2885C29.1898 15.6387 28.7209 14.8274 27.9719 14.8274Z" fill="url(#paint0_linear)"/>
      <g opacity="0.25">
      <path opacity="0.25" d="M27.9719 14.8274H16.6057C16.1018 14.8274 15.6348 15.0978 15.3838 15.5356L11.9382 21.565C11.8973 21.635 11.8234 21.6798 11.7417 21.6798H11.4323C10.6697 21.6798 9.96341 21.2732 9.58207 20.6117L9.4809 20.4366C9.44004 20.3665 9.44004 20.2809 9.4809 20.2109L12.9382 14.1367C13.1873 13.7028 13.1873 13.1698 12.9382 12.7339L9.45561 6.67534C9.41475 6.6053 9.41475 6.51969 9.45561 6.44965L9.58207 6.22981C9.96146 5.5722 10.6638 5.16751 11.4226 5.16751H11.6269C11.7067 5.16751 11.7806 5.21031 11.8214 5.27841L15.3644 11.302C15.6154 11.7397 16.0823 12.0102 16.5862 12.0102H27.9719C28.7209 12.0102 29.1898 11.1989 28.8143 10.551L23.7266 1.74909L23.7168 1.73157C23.711 1.72185 23.7052 1.71213 23.6993 1.7024C23.6429 1.60706 23.5826 1.51561 23.5184 1.42806C22.8705 0.538924 21.8277 0 20.7148 0H9.20851C9.06843 0 8.92835 0.00973084 8.79021 0.0272412C7.84271 0.140086 6.97886 0.649829 6.41658 1.42223C6.34459 1.51951 6.2765 1.62263 6.21618 1.72769L0.463053 11.693C0.192615 12.16 0.0408592 12.6756 0.00778405 13.197C0.00389286 13.2709 0 13.3468 0 13.4207C0 14.018 0.153703 14.6134 0.463053 15.1484L6.21618 25.1138C6.29595 25.2519 6.38545 25.3842 6.48079 25.5068C7.08198 26.2811 7.98668 26.7734 8.9731 26.8415C9.05092 26.8473 9.13069 26.8492 9.20851 26.8492H9.22992H20.7031C20.9833 26.8492 21.2576 26.8142 21.5222 26.75C22.2849 26.5652 22.9717 26.1216 23.4561 25.4971C23.5456 25.3823 23.6273 25.2616 23.7013 25.1352C23.7071 25.1254 23.7129 25.1177 23.7188 25.1079L23.7227 25.1002L28.8162 16.2885C29.1898 15.6387 28.7209 14.8274 27.9719 14.8274Z" fill="#FF0BD1"/>
      </g>
      <path d="M41.5774 7.65786C43.3615 7.65786 44.9744 8.60732 46.0503 9.93421L48.1321 8.17345C46.313 5.93212 43.8537 4.98267 41.5774 4.98267C36.19 4.98267 33.1821 8.80382 33.1821 13.5511C33.1821 19.0065 37.048 21.958 41.5774 21.958C44.5055 21.958 46.7021 20.598 48.1321 18.8003L46.0503 17.0279C44.9977 18.3431 43.4529 19.2925 41.5774 19.2925C38.408 19.2925 36.1317 16.9598 36.1317 13.5511C36.1336 10.014 38.3516 7.65786 41.5774 7.65786Z" fill="white"/>
      <path d="M55.2744 10.2085C53.8794 10.2085 52.609 10.8837 51.7179 12.0958H51.6829V10.3797H49.0641V21.8062H51.6829V16.3839C51.6829 14.5316 52.8152 13.1016 54.8055 13.1016C54.9884 13.1016 55.2978 13.1133 55.5955 13.1367V10.2202C55.5021 10.2085 55.3873 10.2085 55.2744 10.2085Z" fill="white"/>
      <path d="M62.2533 18.0901H62.2299L59.0742 10.3797H56.3173L60.9614 21.1661L58.901 26.4406H61.6579L67.9383 10.3797H65.1931L62.2533 18.0901Z" fill="white"/>
      <path d="M74.962 10.2085C73.4522 10.2085 72.2051 10.7455 71.3354 11.765H71.2887V10.3817H68.6719V26.4406H71.2907V20.4015H71.3257C72.1603 21.4191 73.3841 21.9911 74.9406 21.9911C78.108 21.9911 80.4758 19.6797 80.4758 16.1465C80.4758 12.6911 78.108 10.2085 74.962 10.2085ZM74.5281 19.6116C72.6642 19.6116 71.1875 18.2283 71.1875 16.1562C71.1875 14.1309 72.5494 12.588 74.5164 12.588C76.3473 12.588 77.8103 13.9616 77.8103 16.1562C77.8103 18.1816 76.462 19.6116 74.5281 19.6116Z" fill="white"/>
      <path d="M85.8184 19.5435C85.0868 19.5435 84.721 19.1194 84.721 17.954V12.6113H88.085V10.3817H84.721V6.29788H82.1023V10.3817H80.7073V12.6113H82.1023V18.1933C82.1023 20.7439 83.3261 21.9911 85.2931 21.9911C86.7912 21.9911 87.513 21.6817 88.1978 21.3159L87.511 19.0513C86.906 19.3703 86.2639 19.5435 85.8184 19.5435Z" fill="white"/>
      <path d="M94.4043 10.2085C90.7893 10.2085 88.3982 12.7359 88.3982 16.1115C88.3982 19.4637 90.7893 21.9911 94.4043 21.9911C97.9044 21.9911 100.399 19.5883 100.399 16.1115C100.399 12.6113 97.9044 10.2085 94.4043 10.2085ZM94.4043 19.6116C92.4373 19.6116 91.0637 18.0668 91.0637 16.1115C91.0637 14.1211 92.4022 12.588 94.4043 12.588C96.2798 12.588 97.7332 14.018 97.7332 16.1115C97.7332 18.1699 96.2584 19.6116 94.4043 19.6116Z" fill="white"/>
      <path d="M109.798 15.127H109.751L104.261 5.1636H101.424V21.8082H104.249V10.6541H104.284L108.837 18.6057H110.689L115.242 10.6541H115.277V21.8082H118.102V5.1636H115.265L109.798 15.127Z" fill="white"/>
      <path d="M125.133 10.2085C121.518 10.2085 119.127 12.7359 119.127 16.1115C119.127 19.4637 121.518 21.9911 125.133 21.9911C128.633 21.9911 131.128 19.5883 131.128 16.1115C131.126 12.6113 128.633 10.2085 125.133 10.2085ZM125.133 19.6116C123.166 19.6116 121.793 18.0668 121.793 16.1115C121.793 14.1211 123.131 12.588 125.133 12.588C127.009 12.588 128.462 14.018 128.462 16.1115C128.46 18.1699 126.985 19.6116 125.133 19.6116Z" fill="white"/>
      <path d="M138.171 10.2085C134.556 10.2085 132.165 12.7359 132.165 16.1115C132.165 19.4637 134.556 21.9911 138.171 21.9911C141.671 21.9911 144.165 19.5883 144.165 16.1115C144.165 12.6113 141.671 10.2085 138.171 10.2085ZM138.171 19.6116C136.204 19.6116 134.83 18.0668 134.83 16.1115C134.83 14.1211 136.169 12.588 138.171 12.588C140.046 12.588 141.5 14.018 141.5 16.1115C141.5 18.1699 140.023 19.6116 138.171 19.6116Z" fill="white"/>
      <path d="M154.377 11.7533H154.321C153.463 10.7358 152.204 10.2085 150.706 10.2085C147.56 10.2085 145.192 12.6911 145.192 16.1446C145.192 19.6797 147.56 21.9891 150.729 21.9891C152.354 21.9891 153.613 21.3724 154.447 20.2731H154.494V21.8063H157V5.1636H154.381V11.7533H154.377ZM151.14 19.6116C149.206 19.6116 147.858 18.1816 147.858 16.1582C147.858 13.9616 149.323 12.5899 151.152 12.5899C153.119 12.5899 154.48 14.1348 154.48 16.1582C154.48 18.2263 153.004 19.6116 151.14 19.6116Z" fill="white"/>
      <defs>
      <linearGradient id="paint0_linear" x1="7.63386" y1="26.9537" x2="21.9971" y2="0.362388" gradientUnits="userSpaceOnUse">
      <stop stop-color="#FF009E"/>
      <stop offset="1" stop-color="#FF8820"/>
      </linearGradient>
      </defs>
      </svg>
    `;

  container.appendChild(cryptoLogo);
}

// UTILS
// return seconds, minutes, hours and days when was feed published
function feedReleasedTime(feedTimestamp) {
  let dateNow = new Date().getTime();

  // get difference timestamp between now and provided argument feedTimestamp
  var differenceInMilis = Math.abs(dateNow - feedTimestamp) / 1000;

  // get days
  var days = Math.floor(differenceInMilis / 86400);

  // get hours
  var hours = Math.floor(differenceInMilis / 3600) % 24;

  // get minutes
  var minutes = Math.floor(differenceInMilis / 60) % 60;

  // get seconds
  var seconds = differenceInMilis % 60;

  return {
    seconds: seconds,
    minutes: minutes,
    hours: hours,
    days: days
  };
}

function formatFeedReleaseTime(feedReleasedTimeObj) {
  const { minutes, hours, days } = feedReleasedTimeObj;

  let result;
  if (days > 0) {
    if (days === 1) {
      result = `${days} day ago`;
    } else {
      result = `${days} days ago`;
    }
  } else if (hours > 0) {
    if (hours === 1) {
      result = `${hours} hour ago`;
    } else {
      result = `${hours} hours ago`;
    }
  } else {
    if (minutes === 1) {
      result = `${minutes} minute ago`;
    } else {
      result = `${minutes} minutes ago`;
    }
  }
  return result;
}

/*
 * get sentiment class (positive / negative) indicating color
 * and bar width calculated in percentage where sentiment
 * is in interval < -10, 10 >
 */
function getSentimentValue(sentiment) {
  let sentimentColor = "sentiment-bar-neutral";
  let sentimentBarWidth = (sentiment / 10) * 100;
  if (sentiment > 0 && sentiment <= 10) {
    sentimentColor = "sentiment-bar-positive";
  } else if (sentiment >= -10 && sentiment < 0) {
    sentimentColor = "sentiment-bar-negative";
  }
  return { sentimentColor, sentimentBarWidth };
}

// returns animation speed in [s] based on feeds items.
function getAnimationSpeed(feeds) {
  let speed = 13;
  const increment = 13;
  const feedsLength = feeds.length;
  return feedsLength === 1 ? 30 : speed + feedsLength * increment;
}

/*
 * bar width is 100px (100%) and is divided into 2 segments
 * where left half 50% is negative and right half of 50% is positive.
 */
function setSentimentBar(sentimentColor, sentimentBar, sentimentBarWidth) {
  // when sentimentBarWidth >= 50 % it's necessary to set max width = 50% due to left is always set to 50%
  if (sentimentColor.includes("positive")) {
    sentimentBar.style.left = 50 + "%";
    if (sentimentBarWidth >= 50) {
      sentimentBar.style.width = 50 + "%";
    } else {
      sentimentBar.style.width = sentimentBarWidth + "%";
    }
  } else if (sentimentColor.includes("negative")) {
    // sentimentBarWidth >= -50 % can be width -20% which it's necessary get positive value using Math.absolute
    // and calculate rest of free space from left.
    if (sentimentBarWidth >= -50) {
      sentimentBar.style.left = sentimentBarWidth + 50 + "%";
      sentimentBar.style.width = Math.abs(sentimentBarWidth) + "%";
    } else {
      // sentimentBarWidth could be -80% but max width is only right negative half of sentiment bar (50%)
      // and then max width 50% is set.
      sentimentBar.style.width = 50 + "%";
      sentimentBar.style.left = 0;
    }
  }
}

/* Add one or more listeners to an element
 ** element - DOM element to add listeners to
 ** eventNames - space separated list of event names, e.g. 'click change'
 ** listener - function to attach for each event as a listener
 */
function addMultipleEventListeners(element, eventNames, callback) {
  eventNames
    .split(" ")
    .forEach(e => element.addEventListener(e, callback, false));
}

function createNewsItem(news, shadowRoot, style) {
  const newsFeedItem = document.createElement("a");
  newsFeedItem.className = "news-feed-item";
  newsFeedItem.target = "_blank";
  newsFeedItem.id = `${news.id}`;
  newsFeedItem.href = `${news.link}`;

  // set multiple events with same property value
  addMultipleEventListeners(newsFeedItem, "mouseup mouseleave click", () => {
    style.setProperty("--animation-state", "running");
  });

  newsFeedItem.onmouseenter = () => {
    style.setProperty("--animation-state", "paused");
  };

  const title = document.createElement("div");
  title.className = "title";
  title.innerText = news.title;
  newsFeedItem.appendChild(title);

  const sentimentBarContainer = document.createElement("div");
  sentimentBarContainer.className = "sentiment-bar-container";
  sentimentBarContainer.id = "sentiment-bar-container";
  newsFeedItem.appendChild(sentimentBarContainer);

  const { sentimentColor, sentimentBarWidth } = getSentimentValue(news.impact);
  const sentimentBar = document.createElement("div");
  sentimentBar.className = "sentiment-bar " + sentimentColor;
  sentimentBar.id = "sentiment-bar" + sentimentColor;
  setSentimentBar(sentimentColor, sentimentBar, sentimentBarWidth);
  sentimentBarContainer.appendChild(sentimentBar);

  const timePublished = document.createElement("div");
  let pubDate = feedReleasedTime(news.timestamp);
  timePublished.innerText = formatFeedReleaseTime(pubDate);
  newsFeedItem.appendChild(timePublished);

  const delimiter = document.createElement("span");
  delimiter.className = "delimiter";
  delimiter.innerHTML = "&#9679;";
  newsFeedItem.appendChild(delimiter);

  return newsFeedItem;
}

class CustomWidget extends HTMLElement {
  constructor() {
    super();
    this.refresh = this.refresh.bind(this);

    this.attachShadow({ mode: "open" });

    const shadowStyle = style.cloneNode(true);
    this.shadowRoot.appendChild(shadowStyle);

    this.style = this.shadowRoot.styleSheets[0].cssRules[0].style;

    this.container = document.createElement("div");
    this.container.className = "news-feed-container";
    this.container.id = "news-feed-container";
    this.shadowRoot.appendChild(this.container);
  }

  async refresh(code) {
    const shadowRoot = this.shadowRoot;
    const container = this.container;
    const style = this.style;

    const assets = await cachedGetAssets();
    const asset = assets.find(a => a.code === code);
    if (!asset) {
      throw new Error("Bad asset code");
    }

    const assetId = asset.id;
    const response = await getNews(assetId);
    const {
      data: {
        daily: { news }
      }
    } = response;

    container.innerHTML = "";

    const newsFeedOne = document.createElement("div");
    newsFeedOne.className = "news-feed-one";
    newsFeedOne.id = "news-feed-one";

    const wrapperAnimationDiv = document.createElement("div");
    wrapperAnimationDiv.className = "container-wrapper";
    wrapperAnimationDiv.id = "container-wrapper";
    container.appendChild(wrapperAnimationDiv);

    news.forEach(news => {
      const newsFeedItem = createNewsItem(news, this.shadowRoot, this.style);
      newsFeedOne.appendChild(newsFeedItem);
    });

    // make duplicate element to ensure infinity scrolling effect
    const newsFeedsTwo = newsFeedOne.cloneNode(true);
    newsFeedsTwo.className = "news-feed-two";
    newsFeedsTwo.id = "news-feed-two";

    wrapperAnimationDiv.appendChild(newsFeedOne);
    wrapperAnimationDiv.appendChild(newsFeedsTwo);

    handleLogoInsertion(container);

    style.setProperty("--animation-duration", `${getAnimationSpeed(news)}s`);
  }

  static get observedAttributes() {
    return ["asset"];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    this.refresh(newValue);
  }
}

customElements.define("custom-widget", CustomWidget);
