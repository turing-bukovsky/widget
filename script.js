// // --- mocked feeds data
// let feeds = [
//   {
//     "__typename": "Article",
//     "base": {
//       "id": "cc5ce55f-a585-5049-b05c-aa6c7513211e",
//       "title": "Bank of China Is Now Starting To Like Bitcoin (BTC)",
//       "source": "https://en.businesstimes.cn/articles/116517/20190805/bitcoin-china-bitcoin-crypto.htm",
//       "pub_date": 1564993920
//     },
//     "sentiment": {
//       "sentiment": 0.3612
//     }
//   },
//   {
//     "__typename": "Article",
//     "base": {
//       "id": "cc5ce55f-a585-5049-b05c-aa6c7513211e",
//       "title": "Bank of China Is Now Starting To Like Bitcoin (BTC)",
//       "source": "https://en.businesstimes.cn/articles/116517/20190805/bitcoin-china-bitcoin-crypto.htm",
//       "pub_date": 1564993920
//     },
//     "sentiment": {
//       "sentiment": 0.3612
//     }
//   }
// ];
//
// // --- functions
//
// function run() {
//   // remove all previous styled (due to on resize event)
//   document.querySelectorAll('style')
//     .forEach(el => el.parentNode.removeChild(el));
//   let containerWidth = document.getElementById("news-feed-container").offsetWidth;
//   let containerFeedsWidth = document.getElementById("news-feed").offsetWidth;
//
//   // custom variable
//   let root = document.documentElement;
//   root.style.setProperty('--from-left', containerWidth + 'px' );
//   root.style.setProperty('--to-right', -containerFeedsWidth + 'px' );
//
//   // css
//   var style = document.createElement('style');
//   style.innerHTML =
//     '@keyframes slide{' +
//     '    0%{' +
//     '        transform: translate3d(var(--from-left), 0, 0);' +
//     '    }' +
//     '    100%{' +
//     '        transform: translate3d(var(--to-right), 0, 0);' +
//     '    }' +
//     '}' +
//     '.news-feed-item {\n' +
//     '-webkit-font-smoothing: antialiased;' +
//     '}' +
//     '';
//
//   // insert created style element
//   var ref = document.querySelector('script');
//   ref.parentNode.insertBefore(style, ref);
// }
//
// // return seconds, minutes, hours and days when was feed published
// function feedReleasedData(feedTimestamp) {
//   let dateNow = Date.now();
//   let dateNowTrimmedFromMilis = Math.floor(dateNow / 1000);
//
//   // get difference timestamp between now and provided argument feedTimestamp
//   var differenceInMilis = Math.abs(dateNowTrimmedFromMilis - feedTimestamp);
//
// // get days
//   var days = Math.floor(differenceInMilis / 86400);
//
// // get hours
//   var hours = Math.floor(differenceInMilis / 3600) % 24;
//
// // get minutes
//   var minutes = Math.floor(differenceInMilis / 60) % 60;
//
// // get seconds
//   var seconds = differenceInMilis % 60;
//
//   return {
//     seconds: seconds,
//     minutes: minutes,
//     hours: hours,
//     days: days
//   };
// }
//
//
//
// // --- document begin
//
// /* TODO
//   calculate animation duration based on feeds count... 1 feed = 13 sec ... every new feed +3 secs
//   i.e. 3 feeds = 13 + 3*3 duration (22 sec)
//  */
//
// // in case the document is already rendered
// if (document.readyState!=='loading') run();
// // modern browsers
// else if (document.addEventListener) document.addEventListener('DOMContentLoaded', run);
// // IE <= 8
// else document.attachEvent('onreadystatechange', function(){
//     if (document.readyState==='complete') run();
//   });
//
// window.addEventListener("resize", run);
//
// let feedReleaseAgo = feedReleasedData(1564993920);
//
// console.log(feedReleaseAgo);
//
